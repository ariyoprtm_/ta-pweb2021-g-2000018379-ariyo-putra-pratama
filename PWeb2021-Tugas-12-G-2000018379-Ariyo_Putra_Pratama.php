<?php

    //Mendeklarasikan & Menampilkan Array
    $arrBahasa = array("Javascript","Java","HTML","PHP","Phyton");
    echo $arrBahasa[4]. "<br>";
    echo $arrBahasa[2]. "<br><br>";
    
    //Menampilkan seluruh isi array dengan for dan foreach
    echo "<b>Menampilkan isi array dengan for </b><br>";
    for ($i=0; $i < count($arrBahasa) ; $i++) 
    { 
        echo $arrBahasa[$i].", ";
    }

    echo "<br><br>";
    echo "<b>Menampilkan isi array dengan foreach </b><br>";
    foreach ($arrBahasa as $program) {
        echo $program."<br>";
    }

    $arrAlat = array();
    $arrAlat[]="Pensil";
    $arrAlat[]="Penghapus";
    $arrAlat[]="Bolpoin";
    $arrAlat[]="Penggaris";
    $arrAlat[]="Pengserut";
    echo $arrAlat[1]. "<br>";
    echo $arrAlat[3]. "<br><br>";
    
     
?>

<?php
    
    //Array assosiatif
    echo "<b>Array asosiatif</b><br>";
    $arrHarga = array("Pensil" => 2500, "Penghapus" => 1500, "Bolpoin" => 5000, "Penggaris" => 5500, "Pengserut" => 2000);
    echo $arrHarga['Penggaris']."<br>";

    $tambah = $arrHarga['Penggaris'] + $arrHarga['Pensil'];
    echo "Harga sebuah penggaris dan sebuah pensil adalah :".$tambah." ";

    $arrHarga = array();
    $arrHarga['Buku'] = 5000;
    $arrHarga['Lem'] = 2500;
    $arrHarga['Gunting'] = 7000;

    echo $arrHarga['Lem']."<br>";
    echo $arrHarga['Buku']."<br><br>";

    
?>

<?php
    $arrNilai = array("Ucup" => 90, "Adit" => 98, "Kipli" => 80, "Dennis" => 85);

    //Menampilkan seluruh isi array  asosiatif dengan Foreach dan while-list
    echo "<b>Menampilkan  isi array asosiatif dengan foreach</b> <br>";
    foreach($arrNilai as $name=>$value)
    {
        echo "Nama $name=$value<br>";
    }
    echo "<br><br>";
?>

<?php
    $arrHarga = array("Pensil" => 2500, "Penghapus" => 1500, "Bolpoin" => 5000, "Penggaris" => 5500, "Pengserut" => 2000);

    echo "<b>Array Sebelum diurutkan</b>";
    echo "<pre>";
    print_r($arrHarga);
    echo "</pre>";

    sort($arrHarga);
    reset($arrHarga);
    echo "<b>Array setelah diurutkan dengan sort()</b>";
    echo "<pre>";
    print_r($arrHarga);
    echo "</pre>";
    
    rsort($arrHarga);
    reset($arrHarga);
    echo "<b>Array setelah diurutkan dengan rsort()</b>";
    echo "<pre>";
    print_r($arrHarga);
    echo "</pre>";
?>

<?php
    $arrNilai = array("Ucup" => 90, "Adit" => 98, "Kipli" => 80, "Dennis" => 85);

    echo "<b><br>Array Sebelum diurutkan</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    asort($arrNilai);
    reset($arrNilai);
    echo "<b>Array setelah diurutkan dengan asort()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    arsort($arrNilai);
    reset($arrNilai);
    echo "<b>Array setelah diurutkan dengan arsort()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    ksort($arrNilai);
    reset($arrNilai);
    echo "<b>Array setelah diurutkan dengan ksort()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";

    krsort($arrNilai);
    reset($arrNilai);
    echo "<b>Array setelah diurutkan dengan krsort()</b>";
    echo "<pre>";
    print_r($arrNilai);
    echo "</pre>";
?>

<?php

    //Mengatur posisi pointer dalam array
    echo "<b><br>Mengatur posisi pointer dalam array</b>";
    $arrBahasa = array("Javascript","Java","HTML","PHP","Phyton");
    
    echo "<pre>";
    print_r($arrBahasa);
    echo "</pre>";
    $mode = current($arrBahasa);
    echo $mode."<br>";
    $mode = next($arrBahasa);
    echo $mode."<br>";
    $mode = current($arrBahasa);
    echo $mode."<br>";
    $mode = prev($arrBahasa);
    echo $mode."<br>";
    $mode = end($arrBahasa);
    echo $mode."<br>";
    $mode = current($arrBahasa);
    echo $mode."<br>";
?>

<?php

    //Mencari elemen array
    echo "<b><br>Mencari elemen array</b>";
    $arrBahasa = array("Javascript","Java","HTML","PHP","Phyton");
    if (in_array("PHP",$arrBahasa)) {
        echo "<br>Terdapat Bahasa Pemrograman PHP di dalam Array";
    }
    else{
        echo "<br>Tidak Terdapat Bahasa Pemrograman PHP di dalam Array";
    }
    echo "<br><br>";
?>

<?php
    //Fungsi tanpa return value & parameter
    echo "<b>Fungsi tanpa return value & parameter<br></b>";
    function cetak_prima(){
        for ($x=1; $x <= 100 ; $x++) { 
            $a = 0;
            for ($y=1; $y <= $x ; $y++) { 
                if ($x % $y == 0) {
                    $a++;
                }
            }
            if ($a == 2) {
                echo $x."<br>";
            }
        }
    }
    cetak_prima();
    echo "<br><br>";
?>

<?php
    //Fungsi tanpa return value tapi dengan parameter
    echo "<b>Fungsi tanpa return value tapi dengan parameter<br></b>";
    function cetak_primawp($awal,$akhir){
        for ($x=$awal; $x <= $akhir ; $x++) { 
            $a = 0;
            for ($y=1; $y <= $x ; $y++) { 
                if ($x % $y == 0) {
                    $a++;
                }
            }
            if ($a == 2) {
                echo $x."<br>";
            }
        }
    }

    $v = 2;
    $w = 50;
    cetak_primawp($v,$w);
    echo "<br><br>";
?>

<?php

    //Fungsi dengan Return Value dan Parameter
    echo "<b>Fungsi dengan return value dan parameter<br></b>";
    function kel_lingkaran($jari){
        return 3.14*2*$jari;
    }

    $r = 14;
    echo "Keliling lingkaran dengan jari-jari $r : ";
    echo kel_lingkaran($r);
    echo "<br><br>";
?>

