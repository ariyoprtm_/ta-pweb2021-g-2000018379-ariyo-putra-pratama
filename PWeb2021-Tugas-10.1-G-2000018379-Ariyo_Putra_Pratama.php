<html>
    <head>
        <title>Listing 10.2.1</title>
        <link rel="stylesheet" href="konversi.css" type="text/css">
    </head>
    <body>
        <div class="kotak">
            <p> Program Konversi Nilai Ke huruf</p>
        <form method="GET" name="pap">
            <input type="text" name="nilai" id="nilai" placeholder="Masukkan Nilai">
            <input type="submit" value="Konversi" name="submit" id="submit">
            <?php
                $n = $_GET['nilai'];
                $nilaihuruf = $_GET['nilai'];                
                $kumpul = $_GET['submit'];

                if (($n >= 80) && ($n <= 100))
                {
                    $nilaihuruf = "A";
                }
                else if (($n >= 76.25) && ($n <= 79.99))
                {
                    $nilaihuruf = "A-";
                }
                else if (($n >= 68.75) && ($n <= 76.24))
                {
                    $nilaihuruf = "B+";
                }
                else if (($n >= 65) && ($n <= 68.74))
                {
                    $nilaihuruf = "B";
                }
                else if (($n >= 62.50) && ($n <= 64.99))
                {
                    $nilaihuruf = "B-";
                }
                else if (($n >= 57.50) && ($n <= 62.49))
                {
                    $nilaihuruf = "C+";
                }
                else if (($n >= 55) && ($n <= 57.49))
                {
                    $nilaihuruf = "C";
                }
                else if (($n >= 51.25) && ($n <= 54.99))
                {
                    $nilaihuruf = "C-";
                }
                else if (($n >= 43.75) && ($n <= 51.24))
                {
                    $nilaihuruf = "D+";
                }
                else if (($n >= 40) && ($n <= 43.74))
                {
                    $nilaihuruf = "D";
                }
                else if (($n >= 0) && ($n <= 39.99))
                {
                    $nilaihuruf = "E";
                }
                else {
                    echo "<center><br>Tidak Terdapat Nilai</center>";
                }
                
                echo "<br><center>Nilai Angka   : $n   <br>";
                echo "Nilai Huruf : $nilaihuruf <br>";
            ?>
        </form>
        </div>
    </body>
</html>