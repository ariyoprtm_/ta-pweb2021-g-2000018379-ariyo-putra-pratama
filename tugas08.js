function Alphabet(nilai, pesan){
    var alphaExp = /^[a-zA-Z]+$/;
    if(nilai.value.match(alphaExp)) {
      return true;
    }
    else {
      alert(pesan);
      nilai.focus();
      return false;
    }
}
function Nomor(nilai, pesan){
    var numberExp = /^[0-9]+$/;
    if(nilai.value.match(numberExp)) {
      return true;
    }
    else {
      alert(pesan);
      nilai.focus();
      return false;
    }
}
function cekEmail(nilai, pesan){
    var email = /^([a-zA-Z0-9_.+-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
    if(nilai.value.match(email)) {
      return true;
    }
    else {
      alert(pesan);
      nilai.focus();
      return false;
    }
}
function Kosong(nilai, pesan){
    if (nilai.value == "") {
        alert(pesan);
        nilai.focus();
        return false;
    }
}
function validasi() {
    Alphabet(document.getElementById('nama'), 'Nama harus Huruf semua!!');
    Nomor(document.getElementById('nim'), 'NIM hanya ber isi Nomor!!');
    Nomor(document.getElementById('telp'), 'Telp. hanya ber isi Nomor!!');
    cekEmail(document.getElementById('mail'), 'Email tidak benar!!');
    Kosong(document.getElementById('status'), 'Status tidak boleh kosong!!');
}